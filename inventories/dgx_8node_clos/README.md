## DGX 8 Node CLOS Topology
### Topology
![topology](topology/topology.svg)

### Cabling Map
#### Compute
![compute spine cabling map](topology/cabling_compute_spine.png)
![compute leaf cabling map](topology/cabling_compute_leaf.png)

#### Storage
![storage cabling map](topology/cabling_storage.png)

<!-- AIR:tour -->
### Configurations
#### gpu-spine01
[/etc/network/interfaces](https://gitlab.com/cumulus-consulting/goldenturtle/cumulus_dgx_reference/-/raw/production/inventories/dgx_8node_clos/config/gpu-spine01/interfaces)  
[/etc/frr/frr.conf](https://gitlab.com/cumulus-consulting/goldenturtle/cumulus_dgx_reference/-/raw/production/inventories/dgx_8node_clos/config/gpu-spine01/frr.conf)  
[/etc/frr/daemons](https://gitlab.com/cumulus-consulting/goldenturtle/cumulus_dgx_reference/-/raw/production/inventories/dgx_8node_clos/config/gpu-spine01/daemons)  
[/etc/cumulus/datapath/traffic.conf](https://gitlab.com/cumulus-consulting/goldenturtle/cumulus_dgx_reference/-/raw/production/inventories/dgx_8node_clos/config/gpu-spine01/traffic.conf)  
[/usr/lib/python2.7/dist-packages/cumulus/__chip_config/mlx/datapath.conf](https://gitlab.com/cumulus-consulting/goldenturtle/cumulus_dgx_reference/-/raw/production/inventories/dgx_8node_clos/config/gpu-spine01/datapath.conf)

#### gpu-spine02
[/etc/network/interfaces](https://gitlab.com/cumulus-consulting/goldenturtle/cumulus_dgx_reference/-/raw/production/inventories/dgx_8node_clos/config/gpu-spine02/interfaces)  
[/etc/frr/frr.conf](https://gitlab.com/cumulus-consulting/goldenturtle/cumulus_dgx_reference/-/raw/production/inventories/dgx_8node_clos/config/gpu-spine02/frr.conf)  
[/etc/frr/daemons](https://gitlab.com/cumulus-consulting/goldenturtle/cumulus_dgx_reference/-/raw/production/inventories/dgx_8node_clos/config/gpu-spine02/daemons)  
[/etc/cumulus/datapath/traffic.conf](https://gitlab.com/cumulus-consulting/goldenturtle/cumulus_dgx_reference/-/raw/production/inventories/dgx_8node_clos/config/gpu-spine02/traffic.conf)  
[/usr/lib/python2.7/dist-packages/cumulus/__chip_config/mlx/datapath.conf](https://gitlab.com/cumulus-consulting/goldenturtle/cumulus_dgx_reference/-/raw/production/inventories/dgx_8node_clos/config/gpu-spine02/datapath.conf)

#### gpu-spine03
[/etc/network/interfaces](https://gitlab.com/cumulus-consulting/goldenturtle/cumulus_dgx_reference/-/raw/production/inventories/dgx_8node_clos/config/gpu-spine03/interfaces)  
[/etc/frr/frr.conf](https://gitlab.com/cumulus-consulting/goldenturtle/cumulus_dgx_reference/-/raw/production/inventories/dgx_8node_clos/config/gpu-spine03/frr.conf)  
[/etc/frr/daemons](https://gitlab.com/cumulus-consulting/goldenturtle/cumulus_dgx_reference/-/raw/production/inventories/dgx_8node_clos/config/gpu-spine03/daemons)  
[/etc/cumulus/datapath/traffic.conf](https://gitlab.com/cumulus-consulting/goldenturtle/cumulus_dgx_reference/-/raw/production/inventories/dgx_8node_clos/config/gpu-spine03/traffic.conf)  
[/usr/lib/python2.7/dist-packages/cumulus/__chip_config/mlx/datapath.conf](https://gitlab.com/cumulus-consulting/goldenturtle/cumulus_dgx_reference/-/raw/production/inventories/dgx_8node_clos/config/gpu-spine03/datapath.conf)

#### gpu-spine04
[/etc/network/interfaces](https://gitlab.com/cumulus-consulting/goldenturtle/cumulus_dgx_reference/-/raw/production/inventories/dgx_8node_clos/config/gpu-spine04/interfaces)  
[/etc/frr/frr.conf](https://gitlab.com/cumulus-consulting/goldenturtle/cumulus_dgx_reference/-/raw/production/inventories/dgx_8node_clos/config/gpu-spine04/frr.conf)  
[/etc/frr/daemons](https://gitlab.com/cumulus-consulting/goldenturtle/cumulus_dgx_reference/-/raw/production/inventories/dgx_8node_clos/config/gpu-spine04/daemons)  
[/etc/cumulus/datapath/traffic.conf](https://gitlab.com/cumulus-consulting/goldenturtle/cumulus_dgx_reference/-/raw/production/inventories/dgx_8node_clos/config/gpu-spine04/traffic.conf)  
[/usr/lib/python2.7/dist-packages/cumulus/__chip_config/mlx/datapath.conf](https://gitlab.com/cumulus-consulting/goldenturtle/cumulus_dgx_reference/-/raw/production/inventories/dgx_8node_clos/config/gpu-spine04/datapath.conf)

#### gpu-leaf01
[/etc/network/interfaces](https://gitlab.com/cumulus-consulting/goldenturtle/cumulus_dgx_reference/-/raw/production/inventories/dgx_8node_clos/config/gpu-leaf01/interfaces)  
[/etc/frr/frr.conf](https://gitlab.com/cumulus-consulting/goldenturtle/cumulus_dgx_reference/-/raw/production/inventories/dgx_8node_clos/config/gpu-leaf01/frr.conf)  
[/etc/frr/daemons](https://gitlab.com/cumulus-consulting/goldenturtle/cumulus_dgx_reference/-/raw/production/inventories/dgx_8node_clos/config/gpu-leaf01/daemons)  
[/etc/cumulus/datapath/traffic.conf](https://gitlab.com/cumulus-consulting/goldenturtle/cumulus_dgx_reference/-/raw/production/inventories/dgx_8node_clos/config/gpu-leaf01/traffic.conf)  
[/usr/lib/python2.7/dist-packages/cumulus/__chip_config/mlx/datapath.conf](https://gitlab.com/cumulus-consulting/goldenturtle/cumulus_dgx_reference/-/raw/production/inventories/dgx_8node_clos/config/gpu-leaf01/datapath.conf)

#### gpu-leaf02
[/etc/network/interfaces](https://gitlab.com/cumulus-consulting/goldenturtle/cumulus_dgx_reference/-/raw/production/inventories/dgx_8node_clos/config/gpu-leaf02/interfaces)  
[/etc/frr/frr.conf](https://gitlab.com/cumulus-consulting/goldenturtle/cumulus_dgx_reference/-/raw/production/inventories/dgx_8node_clos/config/gpu-leaf02/frr.conf)  
[/etc/frr/daemons](https://gitlab.com/cumulus-consulting/goldenturtle/cumulus_dgx_reference/-/raw/production/inventories/dgx_8node_clos/config/gpu-leaf02/daemons)  
[/etc/cumulus/datapath/traffic.conf](https://gitlab.com/cumulus-consulting/goldenturtle/cumulus_dgx_reference/-/raw/production/inventories/dgx_8node_clos/config/gpu-leaf02/traffic.conf)  
[/usr/lib/python2.7/dist-packages/cumulus/__chip_config/mlx/datapath.conf](https://gitlab.com/cumulus-consulting/goldenturtle/cumulus_dgx_reference/-/raw/production/inventories/dgx_8node_clos/config/gpu-leaf02/datapath.conf)

#### gpu-leaf03
[/etc/network/interfaces](https://gitlab.com/cumulus-consulting/goldenturtle/cumulus_dgx_reference/-/raw/production/inventories/dgx_8node_clos/config/gpu-leaf03/interfaces)  
[/etc/frr/frr.conf](https://gitlab.com/cumulus-consulting/goldenturtle/cumulus_dgx_reference/-/raw/production/inventories/dgx_8node_clos/config/gpu-leaf03/frr.conf)  
[/etc/frr/daemons](https://gitlab.com/cumulus-consulting/goldenturtle/cumulus_dgx_reference/-/raw/production/inventories/dgx_8node_clos/config/gpu-leaf03/daemons)  
[/etc/cumulus/datapath/traffic.conf](https://gitlab.com/cumulus-consulting/goldenturtle/cumulus_dgx_reference/-/raw/production/inventories/dgx_8node_clos/config/gpu-leaf03/traffic.conf)  
[/usr/lib/python2.7/dist-packages/cumulus/__chip_config/mlx/datapath.conf](https://gitlab.com/cumulus-consulting/goldenturtle/cumulus_dgx_reference/-/raw/production/inventories/dgx_8node_clos/config/gpu-leaf03/datapath.conf)

#### gpu-leaf04
[/etc/network/interfaces](https://gitlab.com/cumulus-consulting/goldenturtle/cumulus_dgx_reference/-/raw/production/inventories/dgx_8node_clos/config/gpu-leaf04/interfaces)  
[/etc/frr/frr.conf](https://gitlab.com/cumulus-consulting/goldenturtle/cumulus_dgx_reference/-/raw/production/inventories/dgx_8node_clos/config/gpu-leaf04/frr.conf)  
[/etc/frr/daemons](https://gitlab.com/cumulus-consulting/goldenturtle/cumulus_dgx_reference/-/raw/production/inventories/dgx_8node_clos/config/gpu-leaf04/daemons)  
[/etc/cumulus/datapath/traffic.conf](https://gitlab.com/cumulus-consulting/goldenturtle/cumulus_dgx_reference/-/raw/production/inventories/dgx_8node_clos/config/gpu-leaf04/traffic.conf)  
[/usr/lib/python2.7/dist-packages/cumulus/__chip_config/mlx/datapath.conf](https://gitlab.com/cumulus-consulting/goldenturtle/cumulus_dgx_reference/-/raw/production/inventories/dgx_8node_clos/config/gpu-leaf04/datapath.conf)

#### stg-leaf01
[/etc/network/interfaces](https://gitlab.com/cumulus-consulting/goldenturtle/cumulus_dgx_reference/-/raw/production/inventories/dgx_8node_clos/config/stg-leaf01/interfaces)

#### stg-leaf02
[/etc/network/interfaces](https://gitlab.com/cumulus-consulting/goldenturtle/cumulus_dgx_reference/-/raw/production/inventories/dgx_8node_clos/config/stg-leaf02/interfaces)
<!-- AIR:tour -->